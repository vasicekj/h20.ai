package com.example.demo.Enums;

public enum SgmlTags {
    DATE,
    UNKNOWN,
    TOPICS,
    PLACES,
    PEOPLE,
    ORGS,
    EXCHANGES,
    COMPANIES,
    TEXT,
}