package com.example.demo.Enums;

public enum TextTags {
    TITLE,
    DATELINE,
    AUTHOR,
    BODY;
}